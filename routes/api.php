<?php

use App\Http\Controllers\AuthenticatedSessionController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\PackageController;
use App\Http\Controllers\RecipientController;
use App\Http\Controllers\SenderController;
use App\Http\Controllers\SubscriptionController;
use App\Http\Controllers\UserController;
use App\Models\Group;
use App\Models\Recipient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

/*
|--------------------------------------------------------------------------
| User Controller Routes
|--------------------------------------------------------------------------
 */

Route::get('users', [UserController::class, 'index'])->name('users.index');
// Route::get('users/{user:id}', [UserController::class, 'show'])->name('users.show');
Route::get('users/{user:id}/senders', [UserController::class, 'showSenders']);
Route::get('users/{user:id}/groups', [UserController::class, 'showGroups']);
Route::get('users/{user:id}/messages', [UserController::class, 'showMessages']);

Route::post('user/store', [UserController::class, 'store'])->name('user.store');
Route::put('user/{user:id}/update', [UserController::class, 'update'])->name('user.update');
Route::delete('user/{user:id}/delete', [UserController::class, 'destroy'])->name('user.destroy');

/*
|--------------------------------------------------------------------------
| Sender Controller Routes
|--------------------------------------------------------------------------
 */

Route::get('senders', [SenderController::class, 'index'])->name('senders.index');
// Route::get('senders/{sender:id}', [SenderController::class, 'show'])->name('sender.show');
Route::get('senders/{sender:id}/messages', [SenderController::class, 'showMessages'])->name('sender.messages');
Route::get('senders/{sender:id}/subscriptions', [SenderController::class, 'showSubscriptions'])->name('sender.subscriptions');

Route::post('sender/store', [SenderController::class, 'store'])->name('sender.store');
Route::put('sender/{sender:id}/update', [SenderController::class, 'update'])->name('sender.update');
Route::delete('sender/{sender:id}/delete', [SenderController::class, 'destroy'])->name('sender.destroy');

/*
|--------------------------------------------------------------------------
| Recipient Controller Routes
|--------------------------------------------------------------------------
 */

Route::get('recipients', [RecipientController::class, 'index'])->name('recipients.index');
// Route::get('recipients/{recipient:id}', [RecipientController::class, 'show'])->name('recipient.show');
Route::get('recipients/{recipient:id}/messages', [RecipientController::class, 'showMessages'])->name('recipient.messages');
Route::get('recipients/{recipient:id}/groups', [RecipientController::class, 'showGroups'])->name('recipient.groups');

Route::post('recipient/store', [RecipientController::class, 'store'])->name('recipient.store');
Route::put('recipient/{recipient:id}/update', [RecipientController::class, 'update'])->name('recipient.update');
Route::delete('recipient/{recipient:id}/delete', [RecipientController::class, 'destroy'])->name('recipient.destroy');

/*
|--------------------------------------------------------------------------
| Group Controller Routes
|--------------------------------------------------------------------------
 */

Route::get('groups', [GroupController::class, 'index'])->name('groups.index');
Route::get('groups/{group:id}/recipients', [GroupController::class, 'showGroupRecipients'])->name('group.recipients');

Route::post('group/store', [GroupController::class, 'store'])->name('group.store');
Route::put('group/{group:id}/update', [GroupController::class, 'update'])->name('group.update');
Route::delete('group/{group:id}/delete', [GroupController::class, 'destroy'])->name('group.destroy');

/*
|--------------------------------------------------------------------------
| Message Controller Routes
|--------------------------------------------------------------------------
 */

Route::get('messages', [MessageController::class, 'index'])->name('messages.index');
Route::get('messages/{message:id}/recipients', [MessageController::class, 'showRecipients']);

Route::post('message/store', [MessageController::class, 'store'])->name('message.store');
Route::put('message/{message:id}/update', [MessageController::class, 'update'])->name('message.update');
Route::delete('message/{message:id}/delete', [MessageController::class, 'destroy'])->name('message.destroy');

/*
|--------------------------------------------------------------------------
| Package Controller Routes
|--------------------------------------------------------------------------
 */

Route::get('packages', [PackageController::class, 'index'])->name('packages.index');
Route::get('packages/{package:id}/subscriptions', [PackageController::class, 'showSubscriptions'])->name('packages.subscriptions');

Route::post('package/store', [PackageController::class, 'store'])->name('package.store');
Route::put('package/{package:id}/update', [PackageController::class, 'update'])->name('package.update');
Route::delete('package/{package:id}/delete', [PackageController::class, 'destroy'])->name('package.destroy');

/*
|--------------------------------------------------------------------------
| Subscription Controller Routes
|--------------------------------------------------------------------------
 */

Route::get('subscriptions', [SubscriptionController::class, 'index'])->name('subscriptions.index');
Route::get('subscriptions/{subscription:id}/packages', [SubscriptionController::class, 'showPackages'])->name('subscriptions.packages');

Route::post('subscription/store', [SubscriptionController::class, 'store'])->name('subscription.store');
Route::put('subscription/{subscription:id}/update', [SubscriptionController::class, 'update'])->name('subscription.update');
Route::delete('subscription/{subscription:id}/delete', [SubscriptionController::class, 'destroy'])->name('subscription.destroy');






