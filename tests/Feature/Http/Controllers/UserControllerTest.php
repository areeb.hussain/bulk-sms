<?php

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(RefreshDatabase::class);

it('creates a user', function () {

    $userData = [
        'name' => 'Test User',
        'email' => 'testcustomer@example.com',
        'password' => 'password',
    ];

    $response = $this->postJson(route('user.store'), $userData);

    $response->assertCreated();

    $userId = $response->json('id');
    $user = User::find($userId);

    expect($user)->not->toBeNull()
        ->name->toBe('Test User')
        ->email->toBe('testcustomer@example.com');
});

it('loads all users', function () {

    User::factory()->count(1)->create();

    $response = $this->getJson(
        route('users.index')
    )->assertOk();

    $response->assertJsonCount(1);
});

it('updates a user', function () {

    $user = User::factory()->create([
        'name' => 'Original Name',
        'email' => 'original@example.com',
        'password' => Hash::make('password'),
    ]);

    $updateData = [
        'name' => 'Updated Name',
        'email' => 'updated@example.com',
    ];

    $response = $this->putJson(route('user.update', ['user' => $user->id]), $updateData);

    $response->assertOk();

    $updatedUser = User::find($user->id);

    expect($updatedUser)->not->toBeNull()
        ->name->toBe('Updated Name')
        ->email->toBe('updated@example.com');
});

it('deletes a user', function () {
    $user = User::factory()->create();

    $this->deleteJson(route('user.destroy', ['user' => $user->id]))
        ->assertStatus(204);

    $deletedUser = User::find($user->id);
    expect($deletedUser)->toBeNull();
});

