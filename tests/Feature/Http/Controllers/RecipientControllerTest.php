<?php

use App\Models\Recipient;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(RefreshDatabase::class);


it('creates a recipient', function () {

    $recipientData = [
        'country_code_id' => 1,
        'phone_number' => '9948882',
    ];

    $response = $this->postJson(route('recipient.store'), $recipientData);

    $response->assertCreated();

    $recipientId = $response->json('id');
    $user = Recipient::find($recipientId);

    expect($user)->not->toBeNull()
        ->country_code_id->toBe(1)
        ->phone_number->toBe('9948882');
});

it('loads all recipients', function () {
    Recipient::factory()->count(1)->create();

    $response = $this->getJson(
        route('recipients.index')
    )->assertOk();

    $response->assertJsonCount(1);
});

it('updates a recipient', function () {

    $recipient = Recipient::factory()->create([
        'country_code_id' => 1,
        'phone_number' => '9948882',
    ]);

    $updateData = [
        'country_code_id' => 2,
        'phone_number' => '7482864'
    ];

    $response = $this->putJson(route('recipient.update', ['recipient' => $recipient->id]), $updateData);

    $response->assertOk();

    $updatedRecipient = Recipient::find($recipient->id);

    expect($updatedRecipient)->not->toBeNull()
        ->country_code_id->toBe(2)
        ->phone_number->toBe('7482864');
});

it('deletes a recipient', function () {
    $recipient = Recipient::factory()->create();

    $this->deleteJson(route('recipient.destroy', ['recipient' => $recipient->id]))
        ->assertStatus(204);

    $deletedRecipient = Recipient::find($recipient->id);
    expect($deletedRecipient)->toBeNull();
});
