<?php

use App\Models\Sender;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(RefreshDatabase::class);

it('creates a sender', function () {

    $senderData = [
        'name' => 'Test Sender',
        'phone_number' => '9999999',
    ];

    $response = $this->postJson(route('sender.store'), $senderData);

    $response->assertCreated();

    $senderId = $response->json('id');
    $sender = Sender::find($senderId);

    expect($sender)->not->toBeNull()
        ->name->toBe('Test Sender')
        ->phone_number->toBe('9999999');
});

it('loads all senders', function () {

    User::factory()->count(1)->create();

    $response = $this->getJson(
        route('users.index')
    )->assertOk();

    $response->assertJsonCount(1);
});

it('updates a sender', function () {

    $sender = Sender::factory()->create([
        'name' => 'Original Name',
        'phone_number' => '9948882',
    ]);

    $updateData = [
        'name' => 'Updated Name',
        'phone_number' => '7482864'
    ];

    $response = $this->putJson(route('sender.update', ['sender' => $sender->id]), $updateData);

    $response->assertOk();

    $updatedSender = Sender::find($sender->id);

    expect($updatedSender)->not->toBeNull()
        ->name->toBe('Updated Name')
        ->phone_number->toBe('7482864');
});

it('deletes a user', function () {
    $sender = Sender::factory()->create();

    $this->deleteJson(route('sender.destroy', ['sender' => $sender->id]))
        ->assertStatus(204);

    $deletedSender = Sender::find($sender->id);
    expect($deletedSender)->toBeNull();
});

