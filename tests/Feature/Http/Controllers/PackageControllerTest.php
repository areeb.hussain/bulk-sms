<?php

use App\Models\Group;
use App\Models\Package;
use App\Models\Sender;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(RefreshDatabase::class);

it('loads all packages', function () {

    Group::factory()->count(1)->create();

    $response = $this->getJson(
        route('groups.index')
    )->assertOk();

    $response->assertJsonCount(1);
});

it('creates a package', function () {

    $packageData = [
        'name' => 'Test Group',
        'balance' => '1000',
    ];

    $response = $this->postJson(route('package.store'), $packageData);

    $response->assertCreated();

    $packageId = $response->json('id');
    $package = Package::find($packageId);

    expect($package)->not->toBeNull()
        ->name->toBe('Test Group')
        ->balance->toBe('1000');
});

it('updates a package', function () {

    $package = Package::create([
        'name' => 'Original Name',
        'balance' => '1000'
    ]);

    $updateData = [
        'name' => 'Updated Name',
        'balance' => '2000',
    ];

    $response = $this->putJson(route('package.update', ['package' => $package->id]), $updateData);

    $response->assertOk();

    $updatedPackage = Package::find($package->id);

    expect($updatedPackage)->not->toBeNull()
        ->name->toBe('Updated Name')
        ->balance->toBe('2000');
});

it('deletes a package', function () {
    $package = Package::create([
        'name' => 'Package 1',
        'balance' => '1000'
    ]);

    $this->deleteJson(route('package.destroy', ['package' => $package->id]))
        ->assertStatus(204);

    $packageGroup = Package::find($package->id);
    expect($packageGroup)->toBeNull();
});

