<?php

use App\Models\Group;
use App\Models\Sender;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(RefreshDatabase::class);

it('loads all groups', function () {

    Group::factory()->count(1)->create();

    $response = $this->getJson(
        route('groups.index')
    )->assertOk();

    $response->assertJsonCount(1);
});

it('creates a group', function () {

    $user = User::factory()->count(1)->create();
    $sender = Sender::factory()->count(1)->create();

    $groupData = [
        'sender_id' => $sender->first()->id,
        'user_id' => $user->first()->id,
        'name' => 'Test Group',
        'description' => 'Description',
    ];

    $response = $this->postJson(route('group.store'), $groupData);

    $response->assertCreated();

    $groupId = $response->json('id');
    $group = Group::find($groupId);

    expect($group)->not->toBeNull()
        ->name->toBe('Test Group')
        ->description->toBe('Description');
});

it('updates a group', function () {

    $group = Group::factory()->create([
        'name' => 'Original Name',
        'description' => 'Description',
    ]);

    $updateData = [
        'name' => 'Updated Name',
        'description' => 'New description',
    ];

    $response = $this->putJson(route('group.update', ['group' => $group->id]), $updateData);

    $response->assertOk();

    $updatedGroup = Group::find($group->id);

    expect($updatedGroup)->not->toBeNull()
        ->name->toBe('Updated Name')
        ->description->toBe('New description');
});

it('deletes a group', function () {
    $group = Group::factory()->create();

    $this->deleteJson(route('group.destroy', ['group' => $group->id]))
        ->assertStatus(204);

    $deletedGroup = Group::find($group->id);
    expect($deletedGroup)->toBeNull();
});

it('adds recipients to a group', function () {

});

it('removes recipients from a group', function () {

});

