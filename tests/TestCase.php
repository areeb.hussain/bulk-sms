<?php

namespace Tests;

use App\Models\User;
use GuzzleHttp\Promise\Create;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;



    public function createUser()
    {
        $user = User::factory()->create();
    }
}
