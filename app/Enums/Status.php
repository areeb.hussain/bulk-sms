<?php

namespace App\Enums;

enum Status: string {
    case Draft = 'draft';
    case Sent = 'sent';
}
