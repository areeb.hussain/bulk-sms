<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recipient extends Model
{
    use HasFactory;

    protected $fillable = ['phone_number', 'country_code_id'];

    /*public function users()
    {
        return $this->morphToMany(Group::class, 'groupable');
    }*/

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'group_recipients', 'recipient_id', 'group_id');
    }

    public function messages()
    {
        return $this->belongsToMany(Message::class, 'message_recipients', 'recipient_id', 'message_id')
            ->withPivot('delivered_at');
    }
}
