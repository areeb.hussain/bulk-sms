<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sender extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'phone_number',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_senders', 'sender_id', 'user_id');
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'sender_id');
    }

    public function groups()
    {
        return $this->hasMany(Group::class, 'sender_id');
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class, 'sender_id');
    }
}
