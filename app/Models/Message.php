<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    protected $fillable = ['sender_id', 'message', 'status', 'sent_at'];

    // User that sent the message
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    // Sender that was used to send the message
    public function sender()
    {
        return $this->belongsTo(Sender::class, 'sender_id');
    }

    // Recipients of the message
    public function recipients()
    {
        return $this->belongsToMany(User::class, 'message_recipients', 'message_id', 'recipient_id')
            ->withPivot('delivered_at');
    }
}
