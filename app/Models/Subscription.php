<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    use HasFactory;

    /*public function package()
    {
        return $this->belongsTo(Package::class, 'sender_id');
    }

    public function sender()
    {
        return $this->belongsTo(Sender::class, 'sender_id');
    }*/

    public function sender()
    {
        return $this->belongsTo(Sender::class, 'sender_id');
    }
}
