<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'balance'];

    public function subscriptions()
    {
        return $this->belongsToMany(Subscription::class, 'subscriptions', 'package_id', 'sender_id');
    }
}
