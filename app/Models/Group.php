<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description', 'sender_id', 'user_id'];

    public function recipients()
    {
        return $this->belongsToMany(Recipient::class, 'group_recipients', 'group_id', 'recipient_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function sender()
    {
        return $this->belongsTo(Sender::class, 'sender_id');
    }
}
