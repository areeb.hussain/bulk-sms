<?php

namespace App\Services;

use App\Enums\Status;
use App\Models\Message;
use App\Models\Recipient;
use App\Models\User;

class MessageService
{
    public function createMessage($senderId, $userId, $content)
    {
        return Message::create([
            'sender_id' => $senderId,
            'user_id' => $userId,
            'body' => $content,
            'char' => strlen($content),
            'rate_id' => rand(1, 2),
            'status' => Status::Draft,
        ]);
    }

    public function sendMessage($messageId, $recipients)
    {
        // will be deducted from user balance
        $message = Message::findOrFail($messageId);
        $sender = User::findOrFail($message->sender_id);

        $recipientsCount = count($recipients);

        $cost = 1.5 * $recipientsCount;

        // $userBalance = $sender->balance;

        /* if ($sender->balance < $cost) {
            ddd('not enough balance');
        }*/

        $message->update([
            'status' => Status::Sent,
            'sent_at' => now(),
            'rate_id' => rand(1, 2),
            'recipients_qty' => $recipientsCount,
            'cost' => $cost,
        ]);

        // $newBalance = $userBalance - $cost;

        // $sender->balance -= $cost;
        // $sender->save();

        foreach ($recipients as $recipientId) {
            $recipient = Recipient::find($recipientId);

            if ($recipient) {

                $recipient->messages()->attach($messageId, [
                    // 'status' => MessageStatus::Draft,
                    'delivered_at' => now()
                ]);
            }
        }
    }
}

