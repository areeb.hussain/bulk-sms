<?php

namespace App\Http\Controllers;

use App\Http\Resources\RecipientCollection;
use App\Http\Resources\RecipientResource;
use App\Models\Message;
use App\Models\Recipient;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RecipientController extends Controller
{
    public function index()
    {
        $recipient = Recipient::get();

        return new RecipientCollection($recipient);
    }

    public function showMessages(Recipient $recipient)
    {
        if (!$recipient->exists) {
            return response()->json(['error' => 'Recipient not found'], 404);
        }

        $recipient->load('messages');

        return new RecipientResource($recipient);
    }

    public function showGroups(Recipient $recipient)
    {
        $recipient->load('groups');

        return new RecipientResource($recipient);
    }

    public function store(Request $request) {

        $validatedData = $request->validate([
            'country_code_id' => 'required|int',
            'phone_number' => 'required|string|max:30|unique:recipients',
        ]);

        $user = Recipient::create($validatedData);

        return response()->json($user, 201);
    }

    public function update(Request $request, Recipient $recipient)
    {
        $validated = $request->validate([
            'country_code_id' => 'required|int',
            'phone_number' => 'required|string|max:30|unique:recipients',
        ]);

        $recipient->update($validated);

        return response()->json($recipient, 200);
    }

    public function destroy(Recipient $recipient)
    {
        $recipient->delete();
        return response()->json(null, 204);
    }
}
