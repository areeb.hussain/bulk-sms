<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Models\Sender;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function index()
    {
        $users = User::get();

        // return UserResource::collection($users);
        return new UserCollection($users);
    }

    /*
    public function show(User $user)
    {
        return new UserResource($user);
    }
    */

    public function showSenders(User $user)
    {
        $user->load('senders');

        return new UserResource($user);
    }

    public function showGroups(User $user)
    {
        $user->load('groups');

        return new UserResource($user);
    }

    public function showMessages(User $user)
    {
        // $user::with(['messages'])->get();

        $user->load('messages');

        return new UserResource($user);
    }

    public function store(Request $request) {

        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        $validatedData['password'] = Hash::make($validatedData['password']);

        $user = User::create($validatedData);

        return response()->json($user, 201);
    }

    public function update(Request $request, User $user)
    {
        $validated = $request->validate([
            'name' => 'string|max:255',
            'email' => 'email|unique:users,email,' . $user->id,
        ]);

        $user->update($validated);

        return response()->json($user, 200);
    }

    public function destroy(User $user)
    {
        $user->delete();
        return response()->json(null, 204);
    }
}
