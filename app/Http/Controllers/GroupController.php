<?php

namespace App\Http\Controllers;

use App\Http\Resources\GroupCollection;
use App\Http\Resources\GroupResource;
use App\Http\Resources\SenderResource;
use App\Models\Group;
use App\Models\Sender;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function index()
    {
        $groups = Group::get();

        return new GroupCollection($groups);
    }

    public function showGroupRecipients(Group $group) {
        $group->load('recipients');

        return new SenderResource($group);
    }

    public function store(Request $request) {

        $validatedData = $request->validate([
            'user_id' => 'required',
            'sender_id' => 'required',
            'name' => 'string|max:255',
            'description' => 'string|max:255'
        ]);

        $sender = Group::create($validatedData);

        return response()->json($sender, 201);
    }

    public function update(Request $request, Group $group)
    {
        $validated = $request->validate([
            'name' => 'string|max:255',
            'description' => 'string|max:255'
        ]);

        $group->update($validated);

        return response()->json($group, 200);
    }

    public function destroy(Group $group)
    {
        $group->delete();
        return response()->json(null, 204);
    }
}
