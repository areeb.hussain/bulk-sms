<?php

namespace App\Http\Controllers;

use App\Http\Resources\MessageCollection;
use App\Http\Resources\MessageResource;
use App\Http\Resources\RecipientResource;
use App\Http\Resources\UserCollection;
use App\Models\Recipient;
use App\Models\Message;
use App\Models\Sender;
use App\Models\User;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function index()
    {
        $messsages = Message::get();

        return new MessageCollection($messsages);
    }

    public function showRecipients(Message $message)
    {
        $message->load('recipients');

        return new MessageResource($message);
    }

    public function store(Request $request) {

        $validatedData = $request->validate([
            'body' => 'string|max:255',
            'phone_number' => 'required|string|max:255|unique:recipients'
        ]);

        $sender = Sender::create($validatedData);

        return response()->json($sender, 201);
    }

    public function update(Request $request, Message $message)
    {
        $validated = $request->validate([
            'body' => 'string|max:255',
            'recipients' => 'array'
        ]);

        $message->update($validated);

        return response()->json($message, 200);
    }

    public function destroy(Sender $sender)
    {
        $sender->delete();
        return response()->json(null, 204);
    }
}
