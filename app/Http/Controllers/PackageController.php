<?php

namespace App\Http\Controllers;

use App\Http\Resources\PackageResource;
use App\Models\Package;
use App\Models\Sender;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    public function index()
    {
        return PackageResource::collection(Package::get());
    }

    public function showSubscriptions(Package $package)
    {
        $package->load('subscriptions');

        return new PackageResource($package);
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required',
            'balance' => 'required',
        ]);

        $package = Package::create($validateData);

        return response()->json($package, 201);
    }
    public function update(Request $request, Package $package)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'balance' => 'required',
        ]);

        $package->update($validatedData);

        return response()->json($package, 200);
    }

    public function destroy(Package $package)
    {
        $package->delete();
        return response()->json(null, 204);
    }

}
