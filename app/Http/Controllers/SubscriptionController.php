<?php

namespace App\Http\Controllers;

use App\Http\Resources\SubscriptionResource;
use App\Models\Package;
use App\Models\Subscription;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    public function index()
    {
        return SubscriptionResource::collection(Subscription::get());
    }

    public function showPackages(Subscription $subscription)
    {
        $subscription->load('packages');

        return new SubscriptionResource($subscription);
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'sender_id' => 'required',
            'package_id' => 'required',
            'start_at' => 'required|date',
            'end_at' => 'required|date',
            'balance' => 'required',
            'rewarded' => 'required',
            'recurring' => 'required',
        ]);

        $subscription = Subscription::create($validateData);

        return new SubscriptionResource($subscription);
    }
    public function update(Request $request, Subscription $subscription)
    {
        $validateData = $request->validate([
            'sender_id' => 'required',
            'package_id' => 'required',
            'start_at' => 'required|date',
            'end_at' => 'required|date',
            'balance' => 'required',
            'rewarded' => 'required',
            'recurring' => 'required',
        ]);

        $subscription->update($validateData);

        return new SubscriptionResource($subscription);
    }

    public function destroy(Subscription $subscription)
    {
        $subscription->delete();

        return response()->json(null, 204);
    }
}
