<?php

namespace App\Http\Controllers;

use App\Http\Resources\SenderResource;
use App\Models\Sender;
use Illuminate\Http\Request;

class SenderController extends Controller
{
    public function index()
    {
        $senders = Sender::get();

        return new SenderResource($senders);
    }

    public function showMessages(Sender $sender)
    {
        $sender->load('messages');

        return new SenderResource($sender);
    }

    public function showSubscriptions(Sender $sender)
    {
        $sender->load('subscriptions');

        return new SenderResource($sender);
    }

    public function store(Request $request) {

        $validatedData = $request->validate([
            'name' => 'string|max:255',
            'phone_number' => 'required|string|max:255|unique:recipients'
        ]);

        $sender = Sender::create($validatedData);

        return response()->json($sender, 201);
    }

    public function update(Request $request, Sender $sender)
    {
        $validated = $request->validate([
            'name' => 'string|max:255',
            'phone_number' => 'required|string|max:255|unique:recipients'
        ]);

        $sender->update($validated);

        return response()->json($sender, 200);
    }

    public function destroy(Sender $sender)
    {
        $sender->delete();
        return response()->json(null, 204);
    }
}
