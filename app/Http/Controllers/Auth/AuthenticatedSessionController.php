<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Resources\AuthenticateUserResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AuthenticatedSessionController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        return $this->respondWithToken($user);
    }

    /**
     * Handle an incoming authentication request.
     */
    public function store(LoginRequest $request)
    {
        $request->authenticate();
        $user = $request->user();

        if (app()->environment(['local', 'staging', 'dev'])) {
            $token = $user->createToken('dashboard', ['*'])->plainTextToken;

            return $this->respondWithToken($user, Str::afterLast($token, '|'));
        }

        $request->session()->regenerate();

        return $this->respondWithToken($user);
    }

    /**
     * Destroy an authenticated session.
     */
    public function destroy(Request $request): Response
    {
        Auth::guard('web')->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return response()->noContent();
    }

    protected function respondWithToken($user, $token = null)
    {
        return response()->json([
            'data' => [
                'access_token' => $token ?? null,
                'token_type' => 'bearer',
                'user' => new AuthenticateUserResource($user),
            ],
            'message' => 'Logged In Successfully',
        ]);
    }
}
