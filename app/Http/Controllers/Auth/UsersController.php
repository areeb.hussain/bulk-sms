<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\UsersCreateRequest;
use App\Http\Requests\UsersUpdateRequest;
use App\Http\Resources\UsersResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::query()->simplePaginate();
        return UsersResource::collection($users)->response()->getData(true);
    }

    public function store(UsersCreateRequest $request)
    {
        $user = User::create($request->validated());
        return new UsersResource($user);
    }

    public function update(UsersUpdateRequest $request)
    {
        $user = User::create($request->validated());
        return new UsersResource($user);
    }

    public function changePassword(User $user, Request $request)
    {
        $request->validate(['password' => ['required', 'string']]);
        $user->update(['password' => Hash::make($request->password)]);

        return new UsersResource($user);
    }


}
