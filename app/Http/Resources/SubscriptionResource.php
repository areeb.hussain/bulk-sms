<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SubscriptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'sender_id' => $this->sender_id,
            'package_id' => $this->package_id,
            'start_at' => $this->start_at,
            'end_at' => $this->end_at,
            'balance' => $this->balance,
            'rewarded' => $this->rewarded,
            'recurring' => $this->recurring,
        ];
    }
}
