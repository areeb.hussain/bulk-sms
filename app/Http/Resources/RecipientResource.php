<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class RecipientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'country_code_id' => $this->country_code_id,
            'phone_number' => $this->phone_number,
            'messages' => MessageResource::collection($this->whenLoaded('messages')),
            'groups' => GroupResource::collection($this->whenLoaded('groups')),
        ];
    }
}
