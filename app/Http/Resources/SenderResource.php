<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SenderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'users' => UserResource::collection($this->whenLoaded('users')),
            'groups' => GroupResource::collection($this->whenLoaded('groups')),
            'messages' => MessageResource::collection($this->whenLoaded('messages')),
            'subscriptions' => SubscriptionResource::collection($this->whenLoaded('subscriptions'))
        ];
    }
}
