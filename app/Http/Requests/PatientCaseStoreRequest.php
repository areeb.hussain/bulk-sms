<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Orion\Http\Requests\Request;

class PatientCaseStoreRequest extends Request
{
    public function storeRules(): array
    {
        return [
            'patient_id' => ['required', 'string', 'exists:patients,id'],
            'requestable_id' => ['required', 'string'],
            'requestable_type' => ['required', 'string'],
            'name' => ['required', 'string'],
            'date' => ['required', 'date'],
            'status' => ['sometimes', 'in:pending,completed'],
            'remarks' => ['string'],
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'patient_id' => $this->patient,
            'requestable_id' => Auth::user()->id,
            'requestable_type' => Auth::user()->getMorphClass(),
        ]);
    }
}
