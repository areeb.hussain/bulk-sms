<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectVolunteerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_id' => ['required', 'string', 'exists:projects,id'],
            'volunteer_id' => ['required', 'string', 'exists:volunteers,id'],
            'project_role_id' => ['required', 'string', 'exists:project_roles,id'],
            'start_date' => ['date'],
            'end_date' => ['date'],
            'contribution' => ['string'],
        ];
    }
}
