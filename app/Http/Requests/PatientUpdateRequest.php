<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatientUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'age' => ['required',],
            'occupational' => ['required', 'string'],
            'passport_no' => ['required', 'string'],
            'location' => ['required', 'string',],
            'nationality' => ['required', 'string',],
            'identity_type' => ['required', 'string'],
            'blood_group' => ['required', 'string'],
            'contact_number' => ['required', 'string'],
        ];
    }
}
