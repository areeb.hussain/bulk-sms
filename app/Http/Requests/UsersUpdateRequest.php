<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UsersUpdateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required'],
            'email' => ['required', Rule::unique('users')->ignore($this->user->id)],
            'registerMediaConversionsUsingModelInstance' => ['boolean'],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
