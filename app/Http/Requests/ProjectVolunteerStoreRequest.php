<?php

namespace App\Http\Requests;

use Orion\Http\Requests\Request;

class ProjectVolunteerStoreRequest extends Request
{
    public function storeRules(): array
    {
        return [
            'name' => ['required', 'string'],
            'volunteer_id' => ['required', 'string', 'exists:volunteers,id'],
            'role_name' => ['required', 'string'],
            'start_date' => ['date', 'date_format:Y-m-d'],
            'end_date' => ['date', 'date_format:Y-m-d'],
            'contribution' => ['string'],
        ];
    }

    protected function prepareForValidation()
    {
        return $this->merge([
            'volunteer_id' => $this->volunteer,
        ]);
    }
}
