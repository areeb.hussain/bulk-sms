<?php

namespace App\Http\Requests;

use Orion\Http\Requests\Request;

class ComorbidityStoreRequest extends Request
{
    public function storeRules(): array
    {
        return [
            'name' => ['required', 'string', 'unique:comorbidities,name'],
            'desc' => ['string'],

        ];
    }
}
