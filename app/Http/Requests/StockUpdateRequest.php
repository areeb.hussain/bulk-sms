<?php

namespace App\Http\Requests;

use Deligoez\LaravelModelHashId\Exceptions\UnknownHashIdConfigParameterException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StockUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'inventory_id' => ['required', 'string', 'exists:inventories,id'],
            'type' => ['required', 'string', 'in:dispersal,re-stock'],
        ];
    }

    /**
     * @throws UnknownHashIdConfigParameterException
     */
    protected function prepareForValidation()
    {
        $user = Auth::user();

        $this->merge([
            'requestable_id' => $user->id,
            'requestable_type' => $user->getMorphClass(),
        ]);
    }
}
