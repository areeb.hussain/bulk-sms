<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoleRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required|string|unique:roles,name',
            'guard_name' => 'required|string',
        ];
    }

    protected function prepareForValidation(): void
    {
        $this->merge([
            'guard_name' => 'api',
        ]);
    }

    public function authorize(): bool
    {
        return true;
    }
}
