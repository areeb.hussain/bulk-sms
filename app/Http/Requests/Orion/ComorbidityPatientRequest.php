<?php

namespace App\Http\Requests\Orion;

use Orion\Http\Requests\Request;

class ComorbidityPatientRequest extends Request
{
    public function rue(): array
    {
        return [
            'status' => 'required|in:draft,review',
        ];
    }
}
