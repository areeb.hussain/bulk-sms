<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VolunteerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'nid' => ['required', 'string'],
            'project_id' => ['required', 'string'],
            'identity_type' => ['required', 'string'],
            'contact_number' => ['required', 'string'],
//            'address_name' => ['required', 'string'],
//            'location_id' => ['required', 'string', 'exists:locations,id'],
        ];
    }
}
