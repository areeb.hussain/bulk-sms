<?php

namespace App\Http\Requests;

use App\Models\Volunteer;
use Orion\Http\Requests\Request;

class VolunteerPatientStoreRequest extends Request
{
    public function updateRules(): array
    {
        return [
            'assignable_type' => 'required|string',
            'assignable_id' => 'required|string',
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'assignable_type' => Volunteer::class,
            'assignable_id' => $this->volunteer,
        ]);
    }
}
