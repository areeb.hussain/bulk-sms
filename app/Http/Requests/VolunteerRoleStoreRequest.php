<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VolunteerRoleStoreRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'volunteer_id' => ['required'],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
