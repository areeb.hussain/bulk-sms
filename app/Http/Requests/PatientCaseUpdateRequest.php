<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class PatientCaseUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'patient_id' => ['required', 'integer', 'exists:patients,id'],
            'requestable_id' => ['required', 'string'],
            'requestable_type' => ['required', 'string'],
            'name' => ['required', 'string'],
            'date' => ['required', 'date'],
            'status' => ['required', 'in:pending,completed'],
            'remarks' => ['string'],
        ];
    }

    protected function prepareForValidation()
    {
        $user = Auth::user();

        if ($user) {
            $this->merge([
                'requestable_id' => $user->id,
                'requestable_type' => $user->getMorphClass(),
            ]);
        }
    }
}
