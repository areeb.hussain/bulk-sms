<?php

namespace App\Http\Requests;

use Orion\Http\Requests\Request;

class VaccinationStoreRequest extends Request
{
    public function storeRules(): array
    {
        return [
            'name' => ['required', 'string', 'unique:vaccinations,name'],
            'desc' => ['string'],
        ];
    }
}
