<?php

namespace App\Http\Requests;

use App\Models\Location;
use App\Models\Patient;
use App\Models\Volunteer;
use Deligoez\LaravelModelHashId\Exceptions\UnknownHashIdConfigParameterException;
use Illuminate\Foundation\Http\FormRequest;

class AddressableStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'location_id' => ['required', 'integer', 'exists:locations,id'],
            'addressable_type' => ['required', 'string'],
            'addressable_id' => ['required', 'integer'],
            'type' => ['required', 'in:patient,volunteer'],
        ];
    }

    /**
     * @throws UnknownHashIdConfigParameterException
     */
    protected function prepareForValidation()
    {
        $addressableId = $this->addressable_id;
        $locationId = Location::keyFromHashId($this->location_id);
        $class = $this->type === 'patient' ? Patient::class : Volunteer::class;

        $this->merge([
            'addressable_type' => $class,
            'addressable_id' => $addressableId,
            'location_id' => $locationId,
        ]);
    }
}
