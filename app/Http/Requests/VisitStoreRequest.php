<?php

namespace App\Http\Requests;

use Orion\Http\Requests\Request;

class VisitStoreRequest extends Request
{
    public function storeRules(): array
    {
        return [
            'name' => ['required', 'string'],
            'clinical_findings' => ['sometimes', 'string'],
            'medication' => ['sometimes', 'array'],
            'advice' => ['sometimes', 'string'],
            'need_referral' => ['sometimes'],
            'specialist_referral' => ['sometimes'],
            'diagnosis' => ['sometimes', 'string'],
            'department' => ['sometimes', 'string'],
            'patient_id' => ['required', 'string', 'exists:patients,id'],
            'date' => ['required', 'date'],

        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'patient_id' => $this->patient,
        ]);
    }
}
