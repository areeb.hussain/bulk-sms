<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Orion\Http\Requests\Request;

class StockStoreRequest extends Request
{
    public function storeRules(): array
    {
        return [
            'inventory_id' => ['required', 'string', 'exists:inventories,id'],
            'requestable_id' => ['required'],
            'requestable_type' => ['required'],
            'type' => ['required', 'string', 'in:dispersal,re-stock'],
            'qty' => ['required', 'integer'],
        ];
    }

    protected function prepareForValidation()
    {
        $user = Auth::user();

        if ($user) {
            $this->merge([
                'inventory_id' => $this->inventory,
                'requestable_id' => $user->id,
                'requestable_type' => $user->getMorphClass(),
            ]);
        }
    }
}
