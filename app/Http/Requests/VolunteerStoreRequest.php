<?php

namespace App\Http\Requests;

use Deligoez\LaravelModelHashId\Exceptions\UnknownHashIdConfigParameterException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class VolunteerStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'project_id' => ['required', 'string'],
            'email' => ['required', 'unique:volunteers,email', 'email', 'string'],
            'nid' => ['required', 'string'],
            'identity_type' => ['required', 'string'],
            'contact_number' => ['required', 'string'],
        ];
    }

    /**
     * @throws UnknownHashIdConfigParameterException
     */
    protected function prepareForValidation()
    {
        $user = Auth::user();

        $this->merge([
            'assignable_id' => $user->id,
            'assignable_type' => $user->getMorphClass(),
        ]);
    }
}
