<?php

namespace App\Http\Requests;

use Orion\Http\Requests\Request;

class VaccinationStatusStoreRequest extends Request
{
    public function storeRules(): array
    {
        return [
            'patient_id' => ['required', 'string', 'exists:patients,id'],
            'vaccination_id' => ['required', 'string', 'exists:vaccinations,id'],
            'dose' => ['required', 'string'],
            'date' => ['required', 'date'],
            'remarks' => ['string'],
            'questions' => ['array', 'sometimes'],
            'serial_number' => ['string'],
            'batch' => ['string'],
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'patient_id' => $this->patient,
        ]);
    }
}
