<?php

namespace App\Http\Requests;

use Deligoez\LaravelModelHashId\Exceptions\UnknownHashIdConfigParameterException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class PatientStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'age' => ['required',],
            'occupational' => ['required', 'string'],
            'location' => ['required', 'string',],
            'nationality' => ['required', 'string',],
            'passport_no' => ['required', 'string'],
            'identity_type' => ['required', 'string'],
            'blood_group' => ['sometimes', 'string'],
            'gender' => ['required', 'string', 'in:female,male'],
            'contact_number' => ['required', 'string'],
        ];
    }

    /**
     * @throws UnknownHashIdConfigParameterException
     */
    protected function prepareForValidation()
    {
        $user = Auth::user();

        $this->merge([
            'assignable_id' => $user->id,
            'assignable_type' => $user->getMorphClass(),
        ]);
    }
}
