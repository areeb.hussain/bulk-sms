<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Spatie\Permission\Models\Permission;

class RolePermissionStoreRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'permission_id' => ['required'],
        ];
    }

    protected function passedValidation()
    {
        $this->merge([
            'permission' => Permission::findOrFail($this->permission_id),
        ]);
    }

    public function authorize(): bool
    {
        return true;
    }
}
