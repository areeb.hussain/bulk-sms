<?php

namespace App\Http\Requests;

use Orion\Http\Requests\Request;

class PatientComorbidityStoreRequest extends Request
{
    public function storeRules(): array
    {
        return [
            'patient_id' => ['required', 'string', 'exists:patients,id'],
            'comorbidity_ids' => ['required', 'array', 'exists:comorbidities,id'],
            'date' => ['required', 'date', 'date_format:Y-m-d'],
            'on_medication' => ['required'],
        ];
    }

    protected function prepareForValidation(): void
    {
        $this->merge([
            'patient_id' => $this->patient,
        ]);
    }
}
