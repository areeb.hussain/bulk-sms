<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VaccinationStatusUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'patient_id' => ['required', 'string', 'exists:patients,id'],
            'vaccination_id' => ['required', 'string', 'exists:vaccinations,id'],
            'dose' => ['required', 'string'],
            'date' => ['required', 'date'],
        ];
    }
}
