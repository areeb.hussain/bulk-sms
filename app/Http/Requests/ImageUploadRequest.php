<?php

namespace App\Http\Requests;

use App\Models\Patient;
use App\Models\Volunteer;
use Illuminate\Foundation\Http\FormRequest;

class ImageUploadRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'file' => ['required', 'image'],
            'type' => ['string', 'in:patient,volunteer'],
            'type_id' => ['string'],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
