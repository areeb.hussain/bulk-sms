<?php

namespace App\Http\Requests;

use Orion\Http\Requests\Request;

class CategoryStoreRequest extends Request
{
    public function storeRules(): array
    {
        return [
            'name' => ['required', 'string', 'unique:categories,name'],
            'desc' => ['string'],
        ];
    }
}
