<?php

namespace Database\Seeders;

use App\Models\Package;
use App\Models\Sender;
use App\Models\Subscription;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubscriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $senders = Sender::all();

        foreach ($senders as $sender) {

            $packageId = strval(rand(1, 2));
            $package = Package::find($packageId);

            // if (!$package) { continue; }

            DB::table('subscriptions')->insert([
                'sender_id' => $sender['id'],
                'package_id' => $packageId,
                'start_at' => now(),
                'end_at' => now()->addMonth(1),
                'balance' => $package->balance,
                'rewarded' => '4',
                'recurring' => true,
            ]);
        }
    }
}
