<?php

namespace Database\Seeders;

use App\Models\Message;
use App\Services\MessageService;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $messageService = new MessageService();

        $senderIds = [4, 5, 6, 5];
        $userId = 1;
        $recipientIds = [1, 2, 3, 4];

        foreach ($senderIds as $index => $senderId) {
            $messageNumber = $index + 1;
            $body = "Message draft $messageNumber";

            $messageService->createMessage($senderId, $userId, $body)->save();
        }

        $message = Message::first();

        $messageService->sendMessage($message->id, $recipientIds);
    }
}
