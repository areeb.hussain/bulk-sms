<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $packages = [
            [
                'name' => 'SMS 1000',
                'balance' => '1000'
            ],
            [
                'name' => 'SMS 2000',
                'balance' => '1000'
            ],
        ];

        foreach ($packages as $package) {
            DB::table('packages')->insert([
                'name' => $package['name'],
                'balance' => $package['balance'],
                'created_at' => now(),
            ]);
        }
    }
}
