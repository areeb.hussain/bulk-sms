<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Message;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            PackageSeeder::class,
            CountryCodeSeeder::class,
            SenderSeeder::class,
            UserSeeder::class,
            GroupSeeder::class,
            RecipientSeeder::class,
            MessageSeeder::class,
            SubscriptionSeeder::class,
        ]);
    }
}
