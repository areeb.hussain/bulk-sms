<?php

namespace Database\Seeders;

use App\Models\Recipient;
use App\Models\Group;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RecipientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Recipient::factory()->count(50)->create()->each(function ($contact) {
            $groupIds = Group::inRandomOrder()->take(rand(1, 3))->pluck('id');
            $contact->groups()->attach($groupIds);
        });
    }
}
