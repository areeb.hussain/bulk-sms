<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountryCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = [
            ['name' => 'Maldives', 'code' => '960'],
            ['name' => 'Canada', 'code' => '873'],
        ];

        foreach ($countries as $country) {
            DB::table('country_codes')->insert([
                'name' => $country['name'],
                'code' => $country['code'],
            ]);
        }
    }
}
