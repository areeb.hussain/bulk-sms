<?php

namespace Database\Seeders;

use App\Models\Group;
use App\Models\Recipient;
use App\Models\Sender;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::factory()->count(50)->create()->each(function ($user) {
            $senderIds = Sender::inRandomOrder()->take(rand(1, 3))->pluck('id');
            $user->senders()->attach($senderIds);
        });
    }
}
