<?php

namespace Database\Factories;

use App\Models\Sender;
use App\Models\User;
use Database\Seeders\UserSeeder;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\GroupFactory>
 */
class GroupFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => implode(' ', fake()->unique()->words(2)),
            'description' => fake()->sentence,
            'user_id' => User::factory(),
            'sender_id' => Sender::factory(),
        ];
    }
}
